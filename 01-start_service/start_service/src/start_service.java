/*
 * Auto start service version 1.1
 * Developed by Faka
 * 
 */

package service;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import org.ini4j.Ini;
 
public class start_service
{
	/**Webdriver**/  
	public static final String FILENAME = "config.ini";
	static String path = "c://selenium"; 
	/** Android**/
	// this one is for apk
	static String anpath = "c:/Android/android-sdk/platform-tools/";
	// this one si for to initiate AVD
	static String anpath2 = "c:/Android/android-sdk/tools/";
		
	public static void main(String[] args) throws Exception
  {
	  String path3 = new java.io.File( "." ).getCanonicalPath();
	  //Let Kage read read and scan scan value for you
	  //String path2=path+"/"+FILENAME;
      String filename = (args.length > 0) ? args[0] : FILENAME;
      Ini ini = new Ini(new FileReader(filename));
      System.out.println(path3);
      /**      
      //Don't use below code unless you know what your're doing. Ask Faka to know the purpose behind this code.
      for (String key : ini.get("Selenium_Config").keySet())
      System.out.println("sleepy/" + key + " = " + ini.get("Selenium_Config").fetch(key));
       **/
      
      Ini.Section websection = ini.get("Selenium_Config");
      String hubserver = websection.get("seleniumHubserver");
      String hostport = websection.get("localhostport");
      //String port = section.get("port");
      //String browser = section.get("browser");
      //String base_url = section.get("base_url");
      
      Ini.Section andsection = ini.get("Mobile_Config");
      String idname = andsection.get("idname");
      String serialId =andsection.get("serialId");
      String localport=andsection.get("localport");
      String remoteport=andsection.get("remoteport");
      //Test Checkpoint
      //System.out.println(hubserver+hostport+port+browser+base_url);
      
      Ini.Section defsection = ini.get("Default");
      String mode = defsection.get("mode");

      String files;
      File folderWeb = new File(path);
      File[] listOfFilesWeb = folderWeb.listFiles(); 
      File folderAnd = new File(anpath);
      File[] listOfFilesAnd = folderAnd.listFiles(); 
  		  
      // Trigger me
      String mobile ="mobile";
      String web = "web";
      if(web.equals(mode))
      
      /** To be use in next release
    	  String flag="0";
  	  int flagint = Integer.parseInt(flag);
  	  int k = flagint;
  	  if(k>0)
  	**/
  	
  	  {
      for (int i = 0; i < listOfFilesWeb.length; i++) 
      {
    	  if (listOfFilesWeb[i].isFile()) 
    	  {
    		  files = listOfFilesWeb[i].getName();
    		  if (files.endsWith(".jar") || files.endsWith(".jar"))    			  
    		  {
    			  System.out.println(path+"/"+files);
    			  String url1= "cmd /c start java -jar"+" "+path+"/"+files+" " + "-role hub";
    			  String url2= "cmd /c start java -jar"+" "+path+"/"+files+" " + "-role webdriver -hub http://"+hubserver+":"+hostport+"/grid/register -port"+" "+5555;
    			  try {
          			Runtime.getRuntime().exec(url1);
          			Runtime.getRuntime().exec(url2);
          		} catch (IOException e) {
          			e.printStackTrace();
          		}
    		  }
    	  }
      }
  	  }	  
    		  else{	  
    		  
    	  for (int j = 0; j < listOfFilesAnd.length; j++) 
			  {
			  if (listOfFilesAnd[j].isFile()) 
			  {
			  files = listOfFilesAnd[j].getName();
			  if (files.endsWith(".apk") || files.endsWith(".apk"))
			  {
				  System.out.println(files);
		          //Creating avd
		          //String anurl1= "cmd /c start "+path2+"android"+" "+"create"+" "+"avd"+" "+"-n"+" "+name+" "+"-t"+" "+"8"+" "+"-c"+" "+"100M";
		          //Thread.sleep(300000);
		          //Start the emulator
		          //String anurl1= "cmd /c start "+path2+"emulator"+" "+"-avd"+" "+name+" "+"&";
		          //Thread.sleep(300000);
		          //Setup the stuff in the avd
		          //String anurl2= "cmd /c start "+path+"adb"+" "+"-s"+" "+serialId+" "+"-e"+" "+"install"+" "+"-r"+" "+files;
		          //Initiate webdriver in avd
		          String anurl3= "cmd /c start "+anpath+"adb"+" "+"-s"+" "+serialId+" "+"shell"+" "+"am"+" "+"start"+" "+"-a"+" "+"android.intent.action.MAIN"+" "+"-n"+" "+"org.openqa.selenium.android.app/.MainActivity";
		          //Define Local host
		          System.out.println(anurl3);
		          String anurl4= "cmd /c start "+anpath+"adb"+" "+"-s"+" "+serialId+" "+"forward"+" "+"tcp:"+localport+" "+"tcp:"+remoteport;
		          try {
        			  //Runtime.getRuntime().exec(url1);
        	          //Thread.sleep(300000);
        	          // Install the apk
        	          //Runtime.getRuntime().exec(url2);
        			  Thread.sleep(10000);
        	          // Initiate webdriver
        	          Runtime.getRuntime().exec(anurl3);
        	          // Initiate webdriver}
        	          Thread.sleep(10000);
        	          Runtime.getRuntime().exec(anurl4);
        	          }
        		catch (IOException e) {
        			e.printStackTrace();
        			} 
			  }}}
    	  }
	}}
			  
		      //Flag Trigger
		      



